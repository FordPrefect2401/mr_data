# 1. Import necessary libraries
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


# Add other libraries based on your specific needs
class DataProcessor:
    def __init__(self):
        self.data = None
        self.cleaned_data = None

    # 2. Define functions for different tasks

    ## 2.1 Data Loading
    def load_data(self, file_path):
        try:
            # Assuming the first row contains headers, adjust if not
            df = pd.read_csv(file_path, delimiter=";")
            print(len(df))
            return df

        except FileNotFoundError:
            print(f"Error: File not found at {file_path}")
            return None
        except Exception as e:
            print(f"An error occurred: {e}")
            return None

    ## 2.2 Data Cleaning
    def clean_data(self, data):
        try:
            # Drop duplicate rows
            cleaned_data = data.drop_duplicates()
            print(len(cleaned_data))
            # Additional cleaning steps can be added here if needed
            # For example, handling missing values or other data cleaning operations

            return cleaned_data
        except Exception as e:
            print(f"An error occurred during data cleaning: {e}")
            return None

    ## 2.3 Exploratory Data Analysis (EDA)
    def explore_data(self, data, save_to_file=False):
        """
        Perform exploratory data analysis to gain insights.

        Parameters:
        - data (pd.DataFrame): The input DataFrame.
        - save_to_file (bool): Whether to save the results to a file.

        Returns:
        - None
        """
        try:
            # Basic summary statistics
            summary_stats = data.describe()

            # Mean, mode, and average for all columns
            mean_values = data.mean()
            mode_values = data.mode().iloc[0]
            median_values = data.median()

            print("Summary Statistics:")
            print(summary_stats)
            print("\nMean Values:")
            print(mean_values)
            print("\nMode Values:")
            print(mode_values)
            print("\nMedian Values:")
            print(median_values)

            if save_to_file:
                summary_stats.to_csv("summary_stats.csv")
                mean_values.to_csv("mean_values.csv")
                mode_values.to_csv("mode_values.csv")
                median_values.to_csv("median_values.csv")
        except Exception as e:
            print(f"An error occurred during data exploration: {e}")
    ## 2.4 Data Visualization
    def visualize_data(self, cleaned_data):
        try:
            if cleaned_data is not None:
                for column in cleaned_data.columns:
                    # Plot histograms for numerical columns
                    if pd.api.types.is_numeric_dtype(cleaned_data[column]):
                        plt.figure(figsize=(8, 6))
                        sns.histplot(cleaned_data[column], kde=True)
                        plt.title(f'Histogram for {column}')
                        plt.show()

                    # Plot bar plots for categorical columns
                    elif pd.api.types.is_categorical_dtype(cleaned_data[column]):
                        plt.figure(figsize=(8, 6))
                        sns.countplot(x=column, data=cleaned_data)
                        plt.title(f'Countplot for {column}')
                        plt.show()

                    # Additional plot types can be added based on the column type

        except Exception as e:
            print(f"An error occurred during data visualization: {e}")

    ## 2.5 Data Processing and Analysis
    def analyze_data(self, data, save_to_file=False):
        try:
            # Outlier Detection
            # Identify outliers using Z-score
            z_scores = (data - data.mean()) / data.std()
            outliers = (z_scores > 3) | (z_scores < -3)

            # Display outliers
            print("Outliers:")
            print(outliers.any())

            # Distribution Summary
            print("\nDistribution Summary:")
            print(data.describe())

            # Correlation Analysis
            print("\nCorrelation Matrix:")
            print(data.corr())

            # Categorical Variable Insights
            categorical_columns = data.select_dtypes(include='category').columns
            for column in categorical_columns:
                print(f"\nValue counts for {column}:")
                print(data[column].value_counts(normalize=True))

            # Missing Data Overview
            print("\nMissing Data Overview:")
            print(data.isnull().sum())

            if save_to_file:
                outliers.to_csv("outliers.csv")
                data.describe().to_csv("distribution_summary.csv")
                data.corr().to_csv("correlation_matrix.csv")
                for column in categorical_columns:
                    data[column].value_counts(normalize=True).to_csv(f"value_counts_{column}.csv")
                data.isnull().sum().to_csv("missing_data_overview.csv")

        except Exception as e:
            print(f"An error occurred during data analysis: {e}")

    # 3. Main Function
    def main(self, file_path):
        # 3.1 Load Data

        self.data = self.load_data(file_path)

        # 3.2 Clean Data
        self.cleaned_data = self.clean_data(self.data)

        # 3.3 Explore Data
        #self.explore_data(self.cleaned_data)

        # 3.4 Visualize Data
        self.visualize_data(self.cleaned_data)

        # 3.5 Analyze Data
        #self.analyze_data(self.data)

# 4. Execute the Program
if __name__ == "__main__":
    data_processor = DataProcessor()
    data_processor.main("Fight.csv")

